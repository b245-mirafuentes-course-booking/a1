const express = require('express')
const router = express.Router()

const userController = require("../Controllers/userController.js")
const auth = require("../auth.js")

// [Routes]

//this is responsible for the registration of the user

router.post("/register", userController.userRegistration)

//this route is for the user authentication
router.post("/login", userController.userAuthentication)

//activity
	// this route to get the user's profile
router.get("/details", auth.verify, userController.getProfile)

// route for user enrollment
router.post("/enroll/:courseId", auth.verify, userController.enrollCourse)

module.exports = router