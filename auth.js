const jwt = require("jsonwebtoken")
// User defined string data will be used to create JSON web tokens
// Used in the algo for encrypting our data which makes it difficutl to decode the information without defined secret keyword

const secret = "CourseBookingAPI"
	
	//[Section] JSON web token
		// JSON web token or jwt is a way of securely passing the server to the frontend or the other parts of the server
		// Information is kept secure through the use of the secret code
		// only the system will know the secret code that can decode the encrypted information

// Token Creation
/*
	Analogy:
		Pack the gift/information and provide the secret code for the key.

*/

	// the argument that will be passed to our parameter(user) will be the document/information of our user
module.exports.createAccessToken = (user) => {
	// payload
		// will contain the data that will be passed to other parts of our API
	const data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//.sign() from jwt package will generate a JSON web token
	//Syntax:
		//jwt.sign(payload, secretCode, options)
	return jwt.sign(data, secret, {})
}

//Token verification
/*
	Analogy:
		- Recieved the gift and open the lock to verify if the sender is legitimate and the gift was not tampered
*/

// Middleware functions have access with request object and response object, and the next function indicates that we may proceed with the next step.
module.exports.verify = (request, response, next) => {
	// The token is retrieved from the request headers.
	// This can be proided in postman under
		// Authorization > Bearer Token
	let token = request.headers.authorization
	

	// Token received and is not undefined.

	if(typeof token !== "undefined"){
		// retrieves only token and removes the "Bearer" prefix
		token = token.slice(7, token.length)
		console.log(token)

		// Validate the toke using the "verify" method decrypting the token using the secret code
		// jwt.verify(token, secretOrPrivateKey, [option/callBackFunction])
		return jwt.verify(token, secret, (error, data) => {
			// if JWT is not valid
			if(error){
				return response.send({auth: "Failed"})
			}
			else{
				// The verify method will be used as middleware in the route to verify the token before proceeding too the function that invokes the contoller function.
				next()
			}
		})
	}
	// Token does not exist
	else{
		return response.send({auth: "Failed"})
	}
}

// Token decryption
/*
	Analogy:
		- Open the gift and get the content.
*/

module.exports.decode = (token) => {
	// Token received is not undefined
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return null
			}
			else{
				// The "decode" method is used to obtain information from the JWT
				// jwt.decode(token, [options])
				// return an object with access to the "payload" property which contains user information stored when the token was generated.
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
	// Token does not exist
	else{
		return null
	}
}