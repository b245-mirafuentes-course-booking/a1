// [bcrypt] 
	// npm install bcrypt
	// In our application we will be using this package to demonstrate gow to encrypt data when a user register to our website.
	// The "bcrypt" package is one of the many packages that we can use to encrypt information but it is not commonly recommended because of how simple tha algo is.
	// There are other more advanced encryption packages that can be used
	// Syntax for hashing password
		// bcrypt.hashSync(password, saltRounds)
	// saltRounds is the value provided as the number of salt round that the bcrypt algorith will run in order to encrypt the password

//[JWT - jsnwebtoken package]
	// jSON web tokens is an industry standard for sending information between our application in a secure manner
	// the jsonwebtoke package will allow usto gain access to methods that will help us create JSON web token