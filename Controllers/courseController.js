const mongoose = require('mongoose')
const Course = require('../Models/coursesSchema.js')
const User = require('../Models/userSchema.js')
const auth = require('../auth.js')

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) => {
	let input = request.body
	let adminId = auth.decode(request.headers.authorization)
	if(adminId.isAdmin == true){
		let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		})
		newCourse.save()
		.then(result => {
			return response.send("added course")
		})
		.catch(error => {
			return response.send(error)
		})
	}else{
		return response.send("you are not qualified")
	}

}


// Create a controller wherein it will retrieved all the courses(active/inactive courses)

module.exports.allCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin !== true){
		return response.send("You don't have access to this route")
	}else{
		Course.find({})
		.then(result => {
			return response.send(result)
		})
		.catch(error => {
			return response.send(error)
		})
	}
}

// Create controller wherein it will retrieve course that are active

module.exports.allActiveCourses = (request, response) => {
	Course.find({isActive: true})
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}

/*
	Mini Activity
		1. You are going to create a route wherein it can retrieve all inactive courses.
		2. make sure that the admin users only are the ones that can access thir route
*/
module.exports.allInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin == true){
		Course.find({isActive: false})
		.then(result => {
			return response.send(result)
		})
		.catch(error => {
			return response.send(error)
		})
	}else{
		return response.send("Only admin can retrieve inactive courses")
	}
}

// This controller will get the details of specific course
module.exports.courseDetails = (request, response) => {
	// to get the params from the url
	const courseId = request.params.courseId
	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

// This controller is for updating specific course
/*
	Business logic:
		1. We are going to edit/update the course, that is stored in the params
*/

module.exports.updateCourse = async (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const courseId = request.params.courseId
	const input = request.body

	if(userData.isAdmin !== true){
		return response.send("You dont have access in this page")
	}else{
		await Course.findOne({_id: courseId})
		.then(result => {
			if(result === null){
				return response.send("courseId is invalid, please try again")
			}else{
				let updatedCourse = {
					name: input.name,
					description: input.description,
					price: input.price
				}
				Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
				.then(result => {
					console.log(result)
					return response.send(result)
				})
				.catch(error => {
					return response.send(error)
				})
			}
		})
		.catch(error => {
			return response.send(error)
		})
	}
}

//activity
module.exports.archivingCourse = (request, response) =>{
	const courseId = request.params.courseId
	const userData = auth.decode(request.headers.authorization)
	const input = request.body
	if(userData.isAdmin === true){
		Course.findOne({_id: courseId, isActive: input.isActive})
		.then(result => {
			if(result.isActive === false){
				return response.send(true)
			}else{
				return response.send("error")
			}
		})
		.catch(error => {
			return response.send(error)
		})

	}else{
		return response.send("only admin is allowed to archive the courses")
	}
}