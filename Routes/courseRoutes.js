const express = require('express')
const router = express.Router()
const courseController = require("../Controllers/courseController")
const auth = require("../auth.js")
const userController = require('../Controllers/userController')

// [Route without params]
// Route for creating a course
router.post("/", auth.verify, courseController.addCourse)

// ROute for retieving all courses
router.get("/all", auth.verify, courseController.allCourses)

// Route for retrieving all active courses
router.get("/allActive", courseController.allActiveCourses)

// Route for retriving all inactive courses
router.get("/allInactive", auth.verify, courseController.allInactiveCourses)


// [Route with params]
// Route for retrieving details of specific course
router.get("/:courseId", courseController.courseDetails)

router.put("/update/:courseId", auth.verify, courseController.updateCourse)

//activity
router.put("/archiveCourse/:courseId", auth.verify, courseController.archivingCourse)

module.exports = router